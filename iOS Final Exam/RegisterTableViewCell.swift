//
//  RegisterTableViewCell.swift
//  iOS Final Exam
//
//  Created by gio emiliano on 2019-12-11.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import UIKit

class RegisterTableViewCell: UITableViewCell {

    
    @IBOutlet weak var bg: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var weightLabel: UILabel!
    
    @IBOutlet weak var heightLabel: UILabel!
    
    @IBOutlet weak var BMILabel: UILabel!
    
    @IBOutlet weak var subView1: UIView!
    @IBOutlet weak var subView2: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        bg.layer.cornerRadius = 5
        bg.backgroundColor = UIColor.white
        
        subView2.layer.cornerRadius = 35
        subView1.layer.cornerRadius = 30
        
       // subView2.backgroundColor = .white
        
        
        
        

        // Configure the view for the selected state
    }
    
    func setup(result: BMIResult, unit: Units) {
        
        dateLabel.text = result.getDescriptionDate()
        weightLabel.text = "Weight: " + result.getDescriptionWeight(byUnit: unit)
        heightLabel.text = "Height: " + result.getDescriptionHeight(byUnit: unit)
        BMILabel.text = result.getResultBMI()
        
        setColorByBMI(BMI: result.getRealResultBMI())

//           weight.text =   result.getWeightDescriptionByKG() //result.description()//""//"Weight: /(result.weight)"
//           height.text =  result.getHeightDescriptionByCM()//""//"Height: " + result.height
//           BMI.text = result.getBMIDescription()
       }
    
    func setColorByBMI(BMI: Double) {
        if ( BMI < 16 ) || (BMI > 40) {
            subView2.backgroundColor = Colors().prjlevelReallyBad
            return
        }
        if (( 16 > BMI ) && (BMI < 17)) || ((BMI > 35) && (BMI < 40)) {
            subView2.backgroundColor = Colors().prjlevelBad
            return
        }
        if (( 17 > BMI ) && (BMI < 18.5)) || ((BMI > 30) && (BMI < 35)) {
            subView2.backgroundColor = Colors().prjlevelModerate
            return
        }
        if (( 25 > BMI ) && (BMI < 30))  {
            subView2.backgroundColor = Colors().prjlevelNotGood
            return
        }
        
        subView2.backgroundColor = Colors().prjlevelGood

    }

}
