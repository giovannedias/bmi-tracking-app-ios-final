//
//  Colors.swift
//  iOS Final Exam
//
//  Created by gio emiliano on 2019-12-12.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import Foundation
import Foundation
import UIKit

class Colors {
    
  

    

    
    let prjlightGray =  UIColor(displayP3Red: 234/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1)


    let prjTextWhite =  UIColor(displayP3Red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
    
    
    let prjPlaceholderWhite =  UIColor(displayP3Red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 0.6)
    
    let prjErrorRed = UIColor(displayP3Red: 246/255.0, green: 109/255.0, blue: 109/255.0, alpha: 0.5)

    let prjlevelReallyBad = UIColor(displayP3Red: 232/255.0, green: 111/255.0, blue: 111/255.0, alpha: 1)
    let prjlevelBad = UIColor(displayP3Red: 232/255.0, green: 175/255.0, blue: 128/255.0, alpha: 1)
    
    let prjlevelModerate = UIColor(displayP3Red: 219/255.0, green: 235/255.0, blue: 143/255.0, alpha: 1)
    
    let prjlevelNotGood = UIColor(displayP3Red: 168/255.0, green: 235/255.0, blue: 143/255.0, alpha: 1)

    let prjlevelGood = UIColor(displayP3Red: 116/255.0, green: 237/255.0, blue: 169/255.0, alpha: 1)

    init(){
        
    }
    
}

