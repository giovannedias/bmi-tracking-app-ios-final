//
//  Extensions.swift
//  iOS Final Exam
//
//  Created by gio emiliano on 2019-12-11.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showMessage(title: String, message: String) {
              
              let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
              alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

              self.present(alert, animated: true) {
                  
              }
          }
       
       func getTimestamp()->String{
            let timestamp = Int64(Date().timeIntervalSince1970 * 1000)

            return String(timestamp)

        }
}


