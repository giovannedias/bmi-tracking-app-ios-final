//
//  BMIResult.swift
//  iOS Final Exam
//
//  Created by gio emiliano on 2019-12-11.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import Foundation
class BMIResult {
    
   
    let oneInch = 0.393701
    let onePound = 2.20462

    
    //kg and cm
    var weight: Double = 0.0
    var height: Double = 0.0
    var BMI: Double = 0.0
    var timestamp : String = ""
    
    init(){
        
    }
    
    
    init(withWeight weight: Double, height: Double, timestamp: String){
        self.weight = weight
        self.height = height
        self.timestamp = timestamp
        
    }
    
    func getMaxWeightValue(byUnit unit: Units) -> Double {
        let max = 300.0
        if (unit == .metric) {
             return max
        }
        
        return max * onePound
    }
    
    func setWeightValue(weightValue: Double, byUnit unit: Units) {
        if (unit == .metric) {
             weight = weightValue
        }
        
        weight  = weightValue / onePound
    }
    
    func getWeightValue(byUnit unit: Units)-> Double {
        if (unit == .metric) {
            return weight
        }
        
        return weight * onePound
    }
    
    
    func setHeightValue(heightValue: Double, byUnit unit: Units) {
        if (unit == .metric) {
             height = heightValue
        }
        
        height  = heightValue / oneInch
    }
    
    func getHeightValue(byUnit unit: Units)-> Double {
        if (unit == .metric) {
            return height
        }
        
        return weight * oneInch
    }
    
    
    func getDescriptionWeight(byUnit unit: Units)->String {
        
        if (unit == .metric) {
            let unit = " kg"
            return String(format:"%.0f", weight) + unit
        }
        let unit = " poundes"
        return String(format:"%.0f", weight * onePound) + unit
    }
    
    func getDescriptionHeight(byUnit unit: Units)->String {
        if (unit == .metric) {
            let unit = " cm"
            return String(format:"%.0f", height) + unit
        }
        let unit = " inches"
        return String(format:"%.0f", height * oneInch) + unit
    }
    
    func getDescriptionDate()->String {
        if let doubleTimestamp = TimeInterval(timestamp)  {
            let date = Date(timeIntervalSince1970: doubleTimestamp/1000)
            print(date)
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want

            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = "yyyy-MM-dd" //Specify your format that you want
            let strDate = dateFormatter.string(from: date)
            return strDate

        }
        
        return ""
    }
    
    func getResultBMI()->String {
        let result = (weight / ((height/100) * (height/100)))
        return String(format:"%.2f", result)
    }
    
    func getRealResultBMI()->Double {
           let result = (weight / ((height/100) * (height/100)))
           return result
       }
       
    
    
    
    
    
    
    
    
    
    init(withWeightInKG : Double, heightInCm: Double, BMI: Double) {
        self.weight = withWeightInKG
        self.height = heightInCm
        self.BMI = BMI
        self.timestamp = getTimestamp()
    }
    
    init(json: [String: Any]) {
          if let weight = json["weight"] as? Double {
              self.weight = weight
          }
      
          if let height = json["height"] as? Double {
                 self.height = height
          }
        
        if let BMI = json["BMI"] as? Double {
                   self.BMI = BMI
            }
        
      
          if let timestamp = json["timestamp"] as? String {
                   self.timestamp = timestamp
            }
      }
      
    
    func getWeightDescriptionByKG() -> String {
        
        return String(format:"%.1f", weight) + "  KG"
      
    }
    
    func getHeightDescriptionByCM() -> String {
           
           return String(format:"%.1f", height) + "  CM"
         
    }
       
    func getBMIDescription()->String {
        return "BMI: " + String(format:"%.1f", BMI)

    }
  
    func getWeightInKG()->String {
        
        return "\(weight)"
    }
    
    func getHeightInCM()->String {
        
        return "\(height)"
    }
    
    func toJSON()-> [String: Any]{
        
        let jsonObject: [String: Any]  =
                      [
                           "weight": weight,
                           "height" : height,
                           "BMI" : BMI,
                           "timestamp" : timestamp
                      ]
                  
        return jsonObject
                  
        
    }
    
    
    func description()->String{
        return "weight: \(weight) height: \(height) BMI: \(BMI) Timestamp: \(timestamp)"
    }
  
    
    func getTimestamp()->String{
               let timestamp = Int64(Date().timeIntervalSince1970 * 1000)

               return String(timestamp)

           }
    
}
