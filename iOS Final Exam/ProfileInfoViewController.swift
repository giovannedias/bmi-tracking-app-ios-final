//
//  ProfileInfoViewController.swift
//  iOS Final Exam
//
//  Created by gio emiliano on 2019-12-12.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import UIKit

class ProfileInfoViewController: UIViewController {
    
    var mainViewController = MainViewController()
    
     let red = UIColor(displayP3Red: 246/255.0, green: 109/255.0, blue: 109/255.0, alpha: 0.5)

    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var ageTextField: UITextField!
    
    @IBOutlet weak var doneButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let name = Database().getName()
        let age = Database().getAge()
        
        nameTextField.text = name
        ageTextField.text = age

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mainViewController.viewAppear()
    }
    
    @IBAction func done(_ sender: Any) {
        
        let name = checkName()
        let age = checkAge()
        
        if age != "", name != "" {
            Database().saveUserInfo(name: name, age: age)
            self.dismiss(animated: true, completion: nil)

        }
        
        
    }
    func checkAge()->String {
           if let age = ageTextField.text {
               if age == "" {
                   ageTextField.backgroundColor = red
               }
               return age
               
               
               
           } else {
               ageTextField.backgroundColor = red
           }
           
           return ""
       }
       
    func checkName()->String {
        if let name = nameTextField.text {
            if name == "" {
                nameTextField.backgroundColor = red
            }
            return name
            
            
            
        } else {
            nameTextField.backgroundColor = red
        }
        
        return ""
    }
    
    @IBAction func resetInfo(_ sender: Any) {
        
        
        let alert = UIAlertController(title: title, message: "Sure? 😨", preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        
        alert.addAction(UIAlertAction(title: "Go ahead!",
                                      style:.destructive,
                               handler: {(alert: UIAlertAction!) in
                                
                                Database().resetAll()
                                
        }))


        self.present(alert, animated: true) {
                         
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
