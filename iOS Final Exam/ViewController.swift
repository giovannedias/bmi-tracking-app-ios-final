//
//  ViewController.swift
//  iOS Final Exam
//
//  Created by gio emiliano on 2019-12-11.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import UIKit

enum Units {
      case metric
      case imperial

  }


class ViewController: UIViewController {

  
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var unit : Units = .metric
    @IBOutlet weak var viewMatric: UIView!
    
    @IBOutlet weak var viewImperial: UIView!
    
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var heightTextField: UITextField!
    
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var toogle: UISegmentedControl!
    
    @IBOutlet weak var weightInPounds: UITextField!
    @IBOutlet weak var heightInchesTextField: UITextField!
    @IBOutlet weak var heightFeetTextField: UITextField!
    
    @IBOutlet weak var saveButton: UIButton!
    
    var newResult = BMIResult()
    var allResults : [BMIResult] = []
    
    var isResultEditing = false
    var canSave = false

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        saveButton.isHidden = true
        viewMatric.isHidden = false
          viewImperial.isHidden = true
        

    }
     override func viewWillAppear(_ animated: Bool) {
        
        let userName = Database().getName()

        if userName == "" {
            performSegue(withIdentifier: "SegueEditInfo", sender: nil)
        } else {
            welcomeLabel.text = "Welcome, " + userName + "!"
        }
        
        if unit == .metric {
            toogle.selectedSegmentIndex = 0
        }
        if unit == .imperial {
            toogle.selectedSegmentIndex = 1

        }
        
        loadAllResults()
        
        
        
    }
    
    func loadAllResults(){
        allResults = Database().getAllResults()
        tableView.reloadData()
    }
    
    
    @IBAction func cancel(_ sender: Any) {
        
        clear()
    }
    
    
  
    @IBAction func send(_ sender: Any) {
        
      
        
        print("weight textfield:  \(String(describing: weightTextField.text))")

        if unit == .metric {
            
            if  let weightString = weightTextField.text,
                let heightString = heightTextField.text {
                
                if let weightDouble = Double(weightString),
                    let heightDouble = Double(heightString){
                          
                    
                    
                    let resultBMI = BMI(weightInKg: weightDouble, heightInCm: heightDouble)
                    
                    if (isResultEditing == true) {
                        
                        self.newResult.weight = weightDouble
                        self.newResult.height = heightDouble
                        self.newResult.BMI = resultBMI

                        
                    } else {
                         self.newResult = BMIResult(withWeightInKG: weightDouble, heightInCm: heightDouble, BMI: resultBMI)
                    }
                    
                    print(self.newResult.description())
                    
                    saveReady(ableToSave: true)
                    resultLabel.text = newResult.getBMIDescription()
                }
            }
            
        }
        
        if unit == .imperial {
            
            if let weightString = weightInPounds.text, let heightFeetString = heightFeetTextField.text, let heightInchsString = heightInchesTextField.text {
                
                if  let weightDouble = Double(weightString),
                    let heightFeetDouble = Double(heightFeetString),
                    let heightInchsDouble = Double(heightInchsString) {
                          
                     let result = BMI(weight: weightDouble,
                                      heightFeet: heightFeetDouble,
                                      heightInchs: heightInchsDouble)
                    
                    resultLabel.text = String(result)
                }
            }
            
        }
        
        
      
        
        
    }
    
    @IBAction func changeMetric(_ sender: Any) {
        
          switch toogle.selectedSegmentIndex
          {
          case 0:
            unit = .metric
            viewMatric.isHidden = false
            viewImperial.isHidden = true
          case 1:
            unit = .imperial
            viewMatric.isHidden = true
            viewImperial.isHidden = false
          default:
              break
          }
        
    }
    
    func clear(){
        saveButton.setTitle("Save", for: .normal)
        weightTextField.text = ""
        heightTextField.text = ""
        resultLabel.text = ""
        isEditing = false
        newResult = BMIResult()
        saveReady(ableToSave: false)

        

    }
    
    func saveReady(ableToSave s: Bool){
        if s == true {
            saveButton.isHidden = false

        } else {
            saveButton.isHidden = true

        }
        canSave = s
    }
    @IBAction func save(_ sender: Any) {
        
        if isResultEditing {
            Database().update(result: newResult)

        } else {
            Database().saveResult(result: newResult)

        }
        
        clear()
        loadAllResults()
        
    }
    
    func edit(result: BMIResult){
           isResultEditing = true
          saveButton.setTitle("Update", for: .normal)
        newResult = result
          if unit == .metric {
              weightTextField.text = result.getWeightInKG()
              heightTextField.text = result.getHeightInCM()
          }
      }
    
    
    func BMI(weight: Double, heightFeet: Double, heightInchs: Double)->Double {
        
        let heightInInches = (heightFeet * 12.000) + heightInchs
        
        print("heightInInches: \(heightInInches)")
        return BMI(weight: weight, height: heightInInches, unit: .imperial)
    }
    
    func BMI(weightInKg: Double, heightInCm: Double)->Double {
           
           let heightInMeters = heightInCm/100

           return BMI(weight: weightInKg, height: heightInMeters, unit: .metric)
    }
       
    
    func BMI(weight: Double, height: Double, unit: Units)->Double {
        
        print("weight:  \(weight)")
        print("height: \(height)")
        
        if unit == .metric {

            return weight / (height * height)
        }
        
        if unit == .imperial {

            return weight*703 / (height * height)
        }
        return 0.0
    }
    
}


extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return allResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

        let result = allResults[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultTableViewCell") as! ResutlTableViewCell
        
        cell.setup(result: result)
        
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 100
       }
      
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            
            let result = allResults[indexPath.row]
            Database().delete(result: result)
            loadAllResults()
            
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let result = allResults[indexPath.row]
        edit(result: result)
    }
    
    
}
