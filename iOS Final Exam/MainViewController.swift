//
//  MainViewController.swift
//  iOS Final Exam
//
//  Created by gio emiliano on 2019-12-11.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import UIKit


class MainViewController: UIViewController {
    
    var allResults : [BMIResult] = []

    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var buttonSave: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var BMILabel: UILabel!
    @IBOutlet weak var sliderWeight: UISlider!
    @IBOutlet weak var sliderHeight: UISlider!
    
    @IBOutlet weak var welcomeLabel: UILabel!
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var viewData: UIView!
    
    
    
    var result : BMIResult = BMIResult()
    var isResultBeingEdit = false
    var unit : Units = .metric
    //var weight : Double = 0.0
    //var height : Double = 0.0
    
    var listOfAllResults : [BMIResult] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        view.backgroundColor = Colors().prjlightGray
        
        viewData.layer.cornerRadius = 5
        viewData.backgroundColor = UIColor.white
        
        viewHeader.layer.cornerRadius = 5
        viewHeader.backgroundColor = UIColor.white
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let name = Database().getName()
        if (name == "") {
               performSegue(withIdentifier: "SegueEditProfile", sender: nil)
        }
        
        let vc = UIViewController()
        vc.modalPresentationStyle = .fullScreen
        
       

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
       viewAppear()
        
    }
    
    //ios 13 modal
    func viewAppear(){
        let name = Database().getName()
               if name == "" {
                   welcomeLabel.text = "Please, tell me your name"
               }else {
                   welcomeLabel.text = "Welcome, " + name + "."
               }
               
               reset()
               loadAllResults()
               
    }
   
    

    @IBAction func changeUnit(_ sender: Any) {
        
        switch segmentedControl.selectedSegmentIndex
        {
                 case 0: unit = .metric
                   
                 case 1: unit = .imperial

                 default:
                     break
        }
        
        Database().saveUnit(unit: unit)
        loadAllResults()

        updateValues()
    }
    
     @IBAction func weightSliderChanged(_ sender: Any) {
            let slider = sender as! UISlider
        
            let sliderValue = Double(slider.value)
        
            result.setWeightValue(weightValue: sliderValue, byUnit: unit)

            weightLabel.text = result.getDescriptionWeight(byUnit: unit)
        
            updateBMI()

    }
     
    @IBAction func heightSliderChanged(_ sender: Any) {
        let slider = sender as! UISlider
        
          let sliderValue = Double(slider.value)
              
         result.setHeightValue(heightValue: sliderValue, byUnit: unit)

         heightLabel.text = result.getDescriptionHeight(byUnit: unit)
        
         updateBMI()
    }
    @IBAction func editProfileInfo(_ sender: Any) {
        performSegue(withIdentifier: "SegueEditProfile", sender: nil)
        

    }
    
    @IBAction func save(_ sender: Any) {
        
        if result.weight == 0.0, result.height == 0.0 {
            return
        }
        
        Database().saveUserWeight(weight: result.weight)
        Database().saveUserHeight(height: result.height)

        let newResult = BMIResult()
        
        newResult.weight = result.weight
        newResult.height = result.height
        newResult.BMI = result.getRealResultBMI()
        
        if isResultBeingEdit == false {
            
            newResult.timestamp = getTimestamp()

        } else {
            newResult.timestamp = result.timestamp

        }
        
        Database().saveResult(result: newResult)
        loadAllResults()
        reset()
        
        
    }
    @IBAction func cancel(_ sender: Any) {
        reset()
    }
    
    
    
}

extension MainViewController {
    
    func edit(resultToEdit: BMIResult) {
        
        result.weight = resultToEdit.weight
        result.height = resultToEdit.height
        result.timestamp = resultToEdit.timestamp
        updateValues()
        buttonSave.setTitle("UPDATE", for: .normal)
        isResultBeingEdit = true
    }
    
    func updateBMI(){
       
        BMILabel.text = result.getResultBMI()
        
    }

    
    func updateValues(){
        
       
        sliderWeight.value = Float(result.getWeightValue(byUnit: unit))
        sliderWeight.maximumValue = Float(result.getMaxWeightValue(byUnit: unit))

        sliderHeight.value = Float(result.getHeightValue(byUnit: unit))
        heightLabel.text = result.getDescriptionHeight(byUnit: unit)
        weightLabel.text = result.getDescriptionWeight(byUnit: unit)
        updateBMI()


    }
    
    func reset(){
        self.unit = Database().getSaveUnit()
        if self.unit == .imperial { self.segmentedControl.selectedSegmentIndex = 1}
        result.weight = Database().getSavedWeight()
        result.height = Database().getSavedHeight()
        result.timestamp = ""
        buttonSave.setTitle("SAVE", for: .normal)
        isResultBeingEdit = false
        updateValues()
        

        
    }
    
    
}


extension MainViewController : UITableViewDelegate, UITableViewDataSource {
    
    
    func loadAllResults(){
        listOfAllResults = []
        listOfAllResults = Database().getAllResults()
        tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.listOfAllResults.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 120
     }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RegisterTableViewCell") as! RegisterTableViewCell
        
        let result = listOfAllResults[indexPath.row]
        cell.setup(result: result, unit: unit)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          let result = listOfAllResults[indexPath.row]
        print("Editing result: " + result.description() )
          edit(resultToEdit: result)
      }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            
            let result = listOfAllResults[indexPath.row]
            Database().delete(result: result)
            listOfAllResults.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                // your code here delayed by 0.5 seconds
                self.loadAllResults()

            }
            

        }
    }
    
    
}

extension MainViewController: UIAdaptivePresentationControllerDelegate
   {
     // This is assuming that the segue is a storyboard segue;
     // if you're manually presenting, just see the delegate there.
     public override func prepare(for segue: UIStoryboardSegue, sender: Any?)
     {
       if segue.identifier == "SegueEditProfile" {
       if  let info = segue.destination as? ProfileInfoViewController {
            info.mainViewController = self
        }
         segue.destination.presentationController?.delegate = self;
       }
     }

     public func presentationControllerDidDismiss(
       _ presentationController: UIPresentationController)
     {
              viewAppear()

     }
   }
