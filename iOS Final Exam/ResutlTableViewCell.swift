//
//  ResutlTableViewCell.swift
//  iOS Final Exam
//
//  Created by gio emiliano on 2019-12-11.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import UIKit

class ResutlTableViewCell: UITableViewCell {
    
    var result : BMIResult = BMIResult()

    @IBOutlet weak var weight: UILabel!
    
    @IBOutlet weak var height: UILabel!
    
    @IBOutlet weak var BMI: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(result: BMIResult) {
        weight.text =   result.getWeightDescriptionByKG() //result.description()//""//"Weight: /(result.weight)"
        height.text =  result.getHeightDescriptionByCM()//""//"Height: " + result.height
        BMI.text = result.getBMIDescription()
    }

}
