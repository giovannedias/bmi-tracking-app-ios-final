//
//  Database.swift
//  iOS Final Exam
//
//  Created by gio emiliano on 2019-12-11.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import Foundation


class Database {
    init(){
        
    }
  
    func saveUnit(unit: Units) {
        if unit == .metric {
            UserDefaults.standard.set(0,forKey: "unit")
        }
        if unit == .imperial {
            UserDefaults.standard.set(1,forKey: "unit")
        }
        
        UserDefaults.standard.synchronize()

    }
    
    func getSaveUnit()->Units {
        if let unit = UserDefaults.standard.value(forKey: "unit") as? Int {
            if unit == 1 {return .imperial}
        }
        return .metric
    }
    
    func saveUserWeight(weight: Double) {
        UserDefaults.standard.set(weight,forKey: "weight")
        UserDefaults.standard.synchronize()
    }
    
    func saveUserHeight(height: Double) {
        UserDefaults.standard.set(height,forKey: "height")
        UserDefaults.standard.synchronize()
    }
    
    func getSavedWeight()->Double {
        if let weight = UserDefaults.standard.value(forKey: "weight") as? Double {
            return weight
        }
        
        return 1.0
    }
    
    func getSavedHeight()->Double {
        if let height = UserDefaults.standard.value(forKey: "height") as? Double {
            return height
        }
        
        return 1.0
    }
    
    func saveUserInfo(name: String, age: String) {
        UserDefaults.standard.set(name,forKey: "name")
        UserDefaults.standard.set(age,forKey: "age")
        UserDefaults.standard.synchronize()

    }
    
    func getName()->String {
        
        if let name = UserDefaults.standard.value(forKey: "name") as? String {
            return name
        }

        return ""
    }
    
    func getAge()->String {
        
        if let age = UserDefaults.standard.value(forKey: "age") as? String {
            return age
        }
        
        return ""
    }
    func saveResult(result: BMIResult) {
        
        print(result.toJSON())
        
        UserDefaults.standard.set(result.toJSON(), forKey: result.timestamp)
    }
    
    func update(result: BMIResult) {
        delete(result: result)
        saveResult(result: result)
    }
    
    func delete(result: BMIResult) {
        UserDefaults.standard.removeObject(forKey: result.timestamp)
        UserDefaults.standard.synchronize()
    }
    func getAllResults()->[BMIResult] {
        
        var allResults : [BMIResult] = []
        
        let allKeys = UserDefaults.standard.dictionaryRepresentation().keys
        
        for key in allKeys {
            if let resultJSON = UserDefaults.standard.object(forKey: key) as? [String: Any] {
                let result = BMIResult(json: resultJSON)
                
                print(result.description())
                if (result.timestamp != "") {
                    allResults.append(result)
                }
                
                
                
            }
            
        }
        
        return allResults.sorted { (a, b) -> Bool in
            a.timestamp > b.timestamp
        }
    }
    
    func resetAll(){
        
        var projectKeys : [String] = []
        let allKeys = UserDefaults.standard.dictionaryRepresentation().keys
        
        for key in allKeys {
            if let resultJSON = UserDefaults.standard.object(forKey: key) as? [String: Any] {
                let result = BMIResult(json: resultJSON)
                
                if (result.timestamp != "") {
                    projectKeys.append(key)
                }
                
            }
            
        }
        
        for myKey in projectKeys {
            UserDefaults.standard.removeObject(forKey: myKey)
                   UserDefaults.standard.synchronize()
        }
       
    }
    
}
