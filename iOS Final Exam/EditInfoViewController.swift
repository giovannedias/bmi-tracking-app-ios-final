//
//  EditInfoViewController.swift
//  iOS Final Exam
//
//  Created by gio emiliano on 2019-12-11.
//  Copyright © 2019 Giovanne Emiliano. All rights reserved.
//

import UIKit

class EditInfoViewController: UIViewController {
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func save(_ sender: Any) {
        
        if let name = nameTextField.text, let age = ageTextField.text {
            Database().saveUserInfo(name: name, age: name)

        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
